package org.example.algorithms;

import org.example.utils.ISort;

public class MergeSort implements ISort {
  private String[] array;

  private int left;

  private int right;

  public MergeSort(String[] array, int left, int right) {
    this.array = array;
    this.left = left;
    this.right = right;
  }

  @Override
  public void execute() {
    mergeSort(this.array,left,right);
  }

  private void mergeSort(String[] array, int left, int right) {
    if (left < right) {
      int middle = (left + right) / 2;
      mergeSort(array, left, middle);
      mergeSort(array, middle + 1, right);
      merge(array, left, middle, right);
    }
  }

  private void merge(String[] array, int left, int middle, int right) {
    int n1 = middle - left + 1;
    int n2 = right - middle;

    String[] leftArray = new String[n1];
    String[] rightArray = new String[n2];

    for (int i = 0; i < n1; i++) {
      leftArray[i] = array[left + i];
    }
    for (int j = 0; j < n2; j++) {
      rightArray[j] = array[middle + 1 + j];
    }

    int i = 0, j = 0, k = left;
    while (i < n1 && j < n2) {
      if (leftArray[i].compareTo(rightArray[j]) <= 0) {
        array[k] = leftArray[i];
        i++;
      } else {
        array[k] = rightArray[j];
        j++;
      }
      k++;
    }

    while (i < n1) {
      array[k] = leftArray[i];
      i++;
      k++;
    }

    while (j < n2) {
      array[k] = rightArray[j];
      j++;
      k++;
    }
  }

  public String[] getArray() {
    return array;
  }
}
