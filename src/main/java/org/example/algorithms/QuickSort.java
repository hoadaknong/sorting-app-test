package org.example.algorithms;

import org.example.utils.ISort;
import org.example.utils.Utils;

public class QuickSort implements ISort {
  private String[] array;

  private int low;

  private int high;

  public QuickSort(String[] array, int low, int high) {
    this.array = array;
    this.low = low;
    this.high = high;
  }

  @Override
  public void execute() {
    quickSort(this.array, this.low, this.high);
  }

  private void quickSort(String[] array, int low, int high) {
    if (low < high) {
      int pivotIndex = partition(array, low, high);
      quickSort(array, low, pivotIndex - 1);
      quickSort(array, pivotIndex + 1, high);
    }
  }

  private int partition(String[] array, int low, int high) {
    String pivot = array[high];
    int i = low - 1;
    for (int j = low; j < high; j++) {
      if (array[j].compareTo(pivot) <= 0) {
        i++;
        // Swap array[i] and array[j]
        Utils.swap(this.array, i, j);
      }
    }
    // Swap array[i+1] and array[high] (pivot)
    Utils.swap(this.array, i + 1, high);
    return i + 1;
  }

  public String[] getArray() {
    return array;
  }
}
