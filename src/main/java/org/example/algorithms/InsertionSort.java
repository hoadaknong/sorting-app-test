package org.example.algorithms;

import org.example.utils.ISort;

public class InsertionSort implements ISort {
  private String[] array;

  public InsertionSort(String[] array) {
    this.array = array;
  }

  @Override
  public void execute() {
    int n = this.array.length;
    for (int i = 1; i < n; i++) {
      String key = this.array[i];
      int j = i - 1;
      while (j >= 0 && this.array[j].compareTo(key) > 0) {
        this.array[j + 1] = this.array[j];
        j--;
      }
      this.array[j + 1] = key;
    }
  }

  public String[] getArray() {
    return array;
  }
}
