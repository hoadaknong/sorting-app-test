package org.example.algorithms;

import org.example.utils.ISort;
import org.example.utils.Utils;

public class BubbleSort implements ISort {
  private String[] array;

  public BubbleSort(String[] array) {
    this.array = array;
  }

  public void execute() {
    int n = this.array.length;
    for (int i = 0; i < n - 1; i++) {
      for (int j = 0; j < n - i - 1; j++) {
        if (this.array[j].compareTo(this.array[j + 1]) > 0) {
          Utils.swap(this.array, j, j + 1);
        }
      }
    }
  }

  public String[] getArray() {
    return array;
  }
}
