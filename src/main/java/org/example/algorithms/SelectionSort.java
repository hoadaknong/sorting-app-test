package org.example.algorithms;

import org.example.utils.ISort;
import org.example.utils.Utils;

public class SelectionSort implements ISort {
  private String[] array;

  public SelectionSort(String[] array) {
    this.array = array;
  }

  @Override
  public void execute() {
    int n = this.array.length;
    for (int i = 0; i < n - 1; i++) {
      int minIndex = i;
      for (int j = i + 1; j < n; j++) {
        if (this.array[j].compareTo(this.array[minIndex]) < 0) {
          minIndex = j;
        }
      }
      Utils.swap(this.array, i, minIndex);
    }
  }

  public String[] getArray() {
    return array;
  }
}
