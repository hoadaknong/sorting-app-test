package org.example;

import org.example.algorithms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

public class Main extends JFrame {
  private JTextArea outputTextArea;

  private JButton generateButton;

  private JButton sortButton;

  private long[] timeTaken;

  private BubbleSort bubbleSort;

  private InsertionSort insertionSort;

  private MergeSort mergeSort;

  private QuickSort quickSort;

  private SelectionSort selectionSort;

  private String[] strings;

  public Main() {
    super("Sorting App");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLayout(new BorderLayout());

    outputTextArea = new JTextArea(60, 100);
    outputTextArea.setEditable(false);
    JScrollPane scrollPane = new JScrollPane(outputTextArea);
    add(scrollPane, BorderLayout.CENTER);

    generateButton = new JButton("Generate Array");
    sortButton = new JButton("Sort");

    JPanel buttonPanel = new JPanel();
    buttonPanel.add(generateButton);
    buttonPanel.add(sortButton);
    add(buttonPanel, BorderLayout.SOUTH);

    generateButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        generateAndDisplayArray();
        generateButton.setText("Regenerate Array");
      }
    });

    sortButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        sortAndDisplayResults();
      }
    });

    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  private String[] generateRandomArray() {
    Random random = new Random();
    return Stream.generate(() -> {
              int length = random.nextInt(5) + 1;
              StringBuilder stringBuilder = new StringBuilder();
              for (int j = 0; j < length; j++) {
                char c = (char) (random.nextInt(26) + 'a');
                if (random.nextBoolean()) {
                  c = Character.toUpperCase(c);
                }
                stringBuilder.append(c);
              }
              return stringBuilder.toString();
            })
            .limit(1000)
            .toArray(String[]::new);
  }

  private void generateAndDisplayArray() {
    this.strings = generateRandomArray();
    outputTextArea.setText("Generated Array with 1000 elements:\n");
    for (String element : this.strings) {
      outputTextArea.append(element + "\n");
    }
    timeTaken = new long[5]; // to store time taken for each sorting algorithm
  }

  private void sortAndDisplayResults() {
    String[] array = this.strings;

    outputTextArea.append("\n\nSorted Array (using different sorting algorithms):\n");

    // Bubble Sort
    String[] bubbleSortedArray = array.clone();
    long startTime = System.nanoTime();
    bubbleSort = new BubbleSort(bubbleSortedArray);
    bubbleSort.execute();
    long endTime = System.nanoTime();
    timeTaken[0] = endTime - startTime;
    outputTextArea.append("1. Bubble Sort: \t\t" + Arrays.toString(bubbleSort.getArray()) + "\n");

    // Selection Sort
    String[] selectionSortedArray = array.clone();
    startTime = System.nanoTime();
    selectionSort = new SelectionSort(selectionSortedArray);
    selectionSort.execute();
    endTime = System.nanoTime();
    timeTaken[1] = endTime - startTime;
    outputTextArea.append("2. Selection Sort: \t" + Arrays.toString(selectionSort.getArray()) + "\n");

    // Insertion Sort
    String[] insertionSortedArray = array.clone();
    startTime = System.nanoTime();
    insertionSort = new InsertionSort(insertionSortedArray);
    insertionSort.execute();
    endTime = System.nanoTime();
    timeTaken[2] = endTime - startTime;
    outputTextArea.append("3. Insertion Sort: \t" + Arrays.toString(insertionSort.getArray()) + "\n");

    // Merge Sort
    String[] mergeSortedArray = array.clone();
    startTime = System.nanoTime();
    mergeSort = new MergeSort(mergeSortedArray, 0, mergeSortedArray.length - 1);
    mergeSort.execute();
    endTime = System.nanoTime();
    timeTaken[3] = endTime - startTime;
    outputTextArea.append("4. Merge Sort: \t\t" + Arrays.toString(mergeSort.getArray()) + "\n");

    // Quick Sort
    String[] quickSortedArray = array.clone();
    startTime = System.nanoTime();
    quickSort = new QuickSort(quickSortedArray, 0, quickSortedArray.length - 1);
    quickSort.execute();
    endTime = System.nanoTime();
    timeTaken[4] = endTime - startTime;
    outputTextArea.append("5. Quick Sort: \t\t" + Arrays.toString(quickSort.getArray()) + "\n");

    // Display time taken for each sorting algorithm
    outputTextArea.append("\nTime taken for each sorting algorithm (in nanoseconds):\n");
    outputTextArea.append("1. Bubble Sort: " + timeTaken[0] + "\n");
    outputTextArea.append("2. Selection Sort: " + timeTaken[1] + "\n");
    outputTextArea.append("3. Insertion Sort: " + timeTaken[2] + "\n");
    outputTextArea.append("4. Merge Sort: " + timeTaken[3] + "\n");
    outputTextArea.append("5. Quick Sort: " + timeTaken[4] + "\n");
    outputTextArea.append("=============================================================\n");
  }

  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        new Main();
      }
    });
  }
}