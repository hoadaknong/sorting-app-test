package org.example.utils;

public class Utils {
  public static void swap(String[] arr, int i, int j) {
    String tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
  }
}
